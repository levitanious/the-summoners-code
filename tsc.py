from bottle import route, request, run, template


main = """
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{{ data.getTitle(id) }} | The Summoner's Code</title>
        <style type="text/css">
            #main {
            box-sizing: border-box;
            width: 800px;
            padding: 5px;
            margin: 0px auto;
            border: 6px double;
            }

            #header {
                display: flex;
                /* border: 1px solid; */
            }

            #title {
                /* border: 1px solid; */
                font-weight: bold;
                flex-grow: 1;
            }

            #navigation {
                display: inline-block;
                /* border: 1px solid; */
            }

            #quote {
                text-align: center;
                margin-top: 20px;
                /* border: 1px solid; */
            }

            #author {
                text-align: end;
                margin-bottom: 20px;
                margin-right: 20px;
                /* border: 1px solid; */
            }

            #explanation {
                margin-left: 20px;
                margin-right: 20px;
                margin-bottom: 20px;
                /* border: 1px solid; */
            }
        </style>
    </head>
    <body>
        <div id="main">
            <div id="header">
                <span id="title">
                    {{ data.getTitle(id) }}
                </span>
                <form id="navigation" action="/">
                    <select name="t">
                        % for i in range(9):
                        <option value="{{ str(i + 1) }}"
                        % if i == id:
                        selected
                        % end
                        >{{ data.getTitle(i) }}</option>
                        % end
                    </select>
                    <input type="submit" value="View"/>
                </form>
            </div>

            <div id="quote">
                {{ data.getQuote(id) }}
            </div>

            <div id="author">
                {{ data.getAuthor(id) }}
            </div>

            <div id="explanation">
                {{ data.getExplanation(id) }}
            </div>
        </div>
    </body>
</html>
"""

data = [ \
    ["1: Support Your Team",
     "[Teamwork] is the fuel that allows common people to attain uncommon results.",
     "—  Andrew Carnegie",
     "While we all carry a diverse set of individual ambitions and expectations into a game of League of Legends, once we hit the Field we're a part of a team. For better or worse, our fates are intertwined with that of our teammates. Once the game gets into full swing, you have to make a choice between being a positive force for your team, or contributing to your own demise.\n\nBeing a good team player begins at champion select. Be open minded when considering the needs of your team. If you're the last one to pick, try to fill a niche in your team that hasn't already been filled. If everyone's picked and something stands out as a deficiency in your team composition, try asking for another player to fill the gap, or change roles to embrace that responsibility yourself. Remember, that by taking on a role you don't normally play, you'll learn more about unfamiliar champions and increase your own skill level.\n\nOnce you get in game, try to keep an open line of communication. Warn your teammates if someone is missing from your lane, or if something is placing them in immediate danger. If they're not paying attention to chat you can always try pinging the map. Just remember that one ping is enough! Also, remember that you have to be there to contribute, so don't leave the game or go AFK! Encourage players who are having trouble, and congratulate those who are playing well. And most of all, if you're having a bad game don't take it out on your team!"
    ],

    ["2: Drive Constructive Feedback",
     "When you confront a problem you begin to solve it.",
     "—  Rudy Giuliani",
     "Player feedback is an important force in the decision making process of Riot Games. If you want to make your voice heard, taking the time to let us know how you're feeling about the game is a good place to start. When you give feedback, make sure you take a holistic approach. If you only give negative feedback, you may find that the changes you influence detract from what you initially enjoyed. Moreover, people are simply more likely to listen if you present yourself in a calm, well thought out manner.\n\nThat being said, don't be afraid to tell us if you feel strongly, and why. Try to be straightforward, specific, and always try to make your feedback direct and concise. For instance, saying something along the lines of:\n\n\'I used to love playing Katarina because her skills give her high mobility in lane, but with the latest nerfs to Death Lotus, I no longer feel like I have a strong enough presence in team fights to be viable. I don't think that I'm going to be playing Kat in the future unless she undergoes some revisions.'\n\nIs a much better way of expressing your dismay at a patch than beginning with an irate tirade, then asking for changes to be reverted or attempting to force an alternate solution. Remember that we're listening and making changes every couple of weeks, so, with a little patience, you may find that your issues will work themselves out."
    ],

    ["3: Facilitate Civil Discussion",
     "To disagree, one doesn't have to be disagreeable.",
     "—  Barry Goldwater",
     "As we mentioned earlier, we want you to give feedback, but being part of the community doesn't stop there. Whether you're in chat, in a game, or on the forums, there are plenty of people to meet, and plenty of topics to discuss. Whether you're discussing game balance and champion viability, trying to form a premade team, or just want to express your affection for the legendary and infamous Gentleman Cho'gath, we encourage you to share your thoughts with other players.\n\nWhen you choose to participate in a discussion with the rest of the playerbase, always try to be receptive to another player's point of view. If you keep an open mind, you'd be surprised what valuable information you can glean from your fellow players. Also, be mindful of how you present your point of view. If a player feels strongly on a subject, don't get caught up trying to have the last word. Just state your side and exit the conversation gracefully rather than give them the opportunity to pick a fight."
    ],

    ["4: Enjoy Yourself, but not at Anyone Else's Expense",
     "Short is the joy that guilty pleasure brings.",
     "—  Euripides",
     "Making games is our business, so it should come as no surprise that we want you to have a lot of fun. We want you to get excited, to have tension-filled moments, and to celebrate your success. This doesn't mean that we're okay with you ruining anybody else's day.\n\nRemember, taking a jab at your friend in the middle of the game is a lot different than making a glib remark at a complete stranger. Someone who is unfamiliar with what you consider playful may take your comment as an attack and react unfavorably. If two players on a team start fighting, good communication and teamwork become nearly impossible. Once communication breaks down, the likelihood of victory is drastically diminished. It isn't uncommon for simple, good natured teasing to spiral out of control into a loss, so do yourself a favor and don't run the risk of sabotaging your own success."
    ],

    ["5: Build Relationships",
     "No man is an island...",
     "—  John Donne",
     "League of Legends is a team game, and, as such, familiarity and rapport with the other competitors with whom you play is going to be a big part of your success. With that in mind, it would behoove you to adopt a cordial demeanor and attempt to make friends. If you have fun playing with another player, make use of the end of game lobby to thank that player for the game and send a friend request. The more friendly players that you have at your disposal, the better your chances are of getting a good, friendly game. Also, if you have friends who you think might be a good fit for the game and community, don't hesitate to shoot them an invite. Not only will you earn yourself some awesome swag, you'll have more friends you can call upon when you're having trouble flying solo.\n\nUse the tools at your disposal to try and build a circle of other players of a similar skill level. If you have a relationship with a group of players that you trust, you are much more likely to get good feedback on how you're playing, receive support when learning a new champion, and just have a good time overall."
    ],

    ["6: Show Humility in Victory, and Grace in Defeat",
     "To be humble to superiors is duty, to equals is courtesy, to inferiors is nobility.",
     "—  Benjamin Franklin",
     "Having a great game is one of the biggest joys that League of Legends can bring you. But always bear in mind that when you're relishing that landslide victory there is someone on the receiving end that is probably ripping their hair out. While it's alright to celebrate, make sure that you keep any gloating (or any other mode of self-indulgence) out of all chat. Instead, thank your opponents for the game. After all, despite their best efforts, they just made you a very happy person.\n\nMoreover, if you've just lost, avoid pointing any fingers or deploying excuses. Even if you had a great game, it's not alright to blame your team. You had five opponents in that game, and - seeing as you just lost - chances are that they had something to do with it. We all know that losing can be frustrating, particularly if it's a close game or one that's completely one sided, but nobody likes a sore loser. Instead, thank your opponents for the game, and take a moment at the end of game screen to ask what you could have done better. If you're polite, you might pick up a few pointers that can help you counter your opponent's strategy in the future."
    ],

    ["7: Be Resolute, not Indignant",
     "It is easier to find men who will volunteer to die, than to find those who are willing to endure pain with patience.",
     "—  Julius Caesar",
     "Intrinsic to the idea of competition is the notion that, when our pride is on the line, emotions tend to run high. Every person that we encounter is going to carry a different set of circumstances with them into the game, and therefore is going to have a different level of tolerance for frustration. If you end up in a game with an abusive player, don't lower yourself to their level. Instead, politely ask them to calm down.And remember, even if you're having a bad game, quitting or going AFK just ruins the game for the rest of the players. If someone's really starting to bother you, the mute and ignore commands are always there to resolve the situation.\n\nAnd remember, while nobody likes being insulted, it pays to take a moment to consider the circumstances. Remember that this is a competitive game, and, more often than not, the other player is just venting their frustration. Try not to take it personally. Everyone has a breaking point and everyone rages sometimes. At some point you may find yourself in the other person's shoes."
    ],

    ["8: Leave No Newbie Behind!",
     "Be an opener of doors for such as come after thee.",
     "—  Ralph Waldo Emerson",
     "We all started somewhere, and if we're going to do justice to the people who helped us move up the ladder, we have to start by paying homage to our roots. If you see a player having a bad game, or who clearly doesn't grasp the fundamentals of the genre, try offering some constructive advice. If you do so in a civil and friendly manner, it's likely that they will be receptive. Oftentimes they'll be downright grateful that somebody took the time to let them know how to improve instead of yelling at them.\n\nNever get frustrated by an inexperienced player's performance. At some point, you were just as green as they were, even if it was the day that you downloaded the League of Legends client. Have a little patience, and try and help the player step up to a level where both of you can enjoy the game. At the same time, don't be discouraged if they aren't receptive. Some small percentage of players will get hung up on the notion that they don't need anybody's help, and, no matter how politely you try to lend a hand, they won't want to hear it. That's no reason to give up on the rest of them!"
    ],

    ["9: Lead by Example",
     "Leadership is practiced not so much in words as in attitude and in actions.",
     "—  Harold S. Geneen",
     "If you share our vision of a game where players exercise good sportsmanship, help each other improve and form lasting friendships, you've got to start living the dream before anybody else is willing to do so. It's all well and good to say you're on board for the revolution, but if you don't first make yourself a paragon of model behavior, no one is going to be fooled. Nobody's asking you to be perfect, but we do want you to, whenever possible, strive to uphold the same standards of behavior that you expect everyone else to maintain.\n\nSo, remember! Stay positive, remain calm, and keep to the code!"
    ] \
]


class DB:
    def __init__(self, data):
        self.title = []
        self.quote = []
        self.author = []
        self.explanation = []

        for i in data:
            self.title.append( i[0] )
            self.quote.append( i[1] )
            self.author.append( i[2] )
            self.explanation.append( i[3] )

    def getTitle(self, id):
        return self.title[id]

    def getQuote(self, id):
        return self.quote[id]

    def getAuthor(self, id):
        return self.author[id]

    def getExplanation(self, id):
        return self.explanation[id]


data = DB(data)


@route('/')
def index():
    id = request.query.t or '1'
    id = int(id)
    if 1 <= id <= 9:
        id -= 1
        return template(main, id=id, data=data)
    else:
        return


if __name__ == '__main__':
    run(host='127.0.0.1', port=8080, debug=False, reloader=False)
